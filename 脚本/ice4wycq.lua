--[===[-------------------------------------------------------------------------------------------------

★★★★★★★★★★ 艾斯万月源码备份 ★★★★★★★★★★

----------------------------------------------------------------------------------------------------]===]

-- 准备工作
ICE_System.prepare = function()
	if ICE_UI.show() then   -- 显示UI
		-- 1.初始化
        ICE_Paojy.debug = false
        ICE_Paojy.init(ICE_Const.AppKey, ICE_Const.AppSecret, ICE_Config.apiAddress(), ICE_Const.ProjectCode)
        -- 3.设置卡密
        ICE_Paojy.setCard(ICE_Config.cardNo())
        -- 4.验证卡密
        local code, message = ICE_Paojy.verify()
        if 0 == code then
        	ICE_Utils.showTipsLarge(message)
            sleep(3000)
            if ICE_Login.launchGame() then
				ICE_System.gameSettings()
				ICE_Utils.hideActIcon()
				ICE_System.closeSystemChannel()
			end
        else
        	ICE_Utils.blockLarge(message)
        end
    else
    	exitScript()
	end
end

-- 开始运行脚本
ICE_System.start = function()
	ICE_System.prepare()		-- 初始化
	ICE_Package.checkStone() 	-- 检查石头
	ICE_Utils.setPKMode(ICE_Config.getDroplistValue("pkMode")) -- 设置PK模式
	ICE_OnHook.inplace() 		-- 原地挂机
	ICE_OnHook.hook()			-- 打怪挂机
	ICE_Utils.block("没有开启任何功能，只能站在原地发呆~_~")
end