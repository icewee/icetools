--[===[-------------------------------------------------------------------------------------------------

★★★★★★★★★★ 艾斯华夏源码备份 ★★★★★★★★★★

----------------------------------------------------------------------------------------------------]===]

-- 准备工作
ICE_System.prepare = function()
	Icesoft.update(ICE_Const.version, ICE_Const.HotLink) -- 热更
	if ICE_UI.show() then   -- 显示UI
		-- 1.初始化
		Icesoft.debug = false
		Icesoft.init(ICE_Const.AppKey, ICE_Const.AppSecret, ICE_Config.apiAddress(), ICE_Const.ProjectCode)
		-- 3.设置卡密
		Icesoft.setCard(ICE_Config.cardNo())
		-- 4.验证卡密
		local code, message = Icesoft.verify()
		if 0 == code then
			ICE_Utils.showTipsLarge(message)
			sleep(3000)
			ICE_Login.launchGame()
		else
			ICE_Utils.blockLarge(message)
		end
	else
		exitScript()
	end
end

-- 开始运行脚本
ICE_System.start = function()
	ICE_System.prepare()	    	-- 初始化
	ICE_Extend.born() 				-- 一键起号
	ICE_Extend.mine()				-- 探矿
	ICE_Extend.strippingSoul()	-- 剥离魂魄
    ICE_Extend.oneKeyFengmo()		-- 一键封魔
	ICE_OnHook.setPKMode()		-- 设置PK模式
	ICE_Task.taskDailyAtonce() 	-- 立即日常任务
	ICE_OnHook.hook()				-- 打怪挂机
	ICE_Extend.stand()			-- 原地静默
	ICE_Utils.block("没有开启任何功能，只能站在原地发呆~_~")
end