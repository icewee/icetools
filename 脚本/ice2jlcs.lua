--[===[-------------------------------------------------------------------------------------------------

★★★★★★★★★★ 傲视剑来传世源码备份 ★★★★★★★★★★

----------------------------------------------------------------------------------------------------]===]

-- 准备工作
ICE_System.prepare = function()
	if ICE_UI.show() then   -- 显示UI
		km = ICE_Config.cardNo()
		if ICE_Utils.isBlank(km) then
			ICE_Utils.blockLarge("卡 密 验 证 失 败\n请 输 入 已 购 买 的 卡 密 ！")
		else
			as.退出上次心跳()
			卡密token = as.卡密登陆(km)
			if 卡密token ~= nil then
				as.开启心跳线程(km, 卡密token)
			else
				ICE_Utils.blockLarge("卡 密 验 证 失 败")
			end
		end
		if ICE_Login.launchGame() == false then
			ICE_Utils.blockLarge("加 载 游 戏 失 败 ！")
		end
	else
		exitScript()
	end
end

-- 开始运行脚本
ICE_System.start = function()
	ICE_System.prepare()    		-- 初始化
    ICE_System.gameSettings()	-- 游戏设置
	ICE_Package.checkStone() 		-- 检查石头
	ICE_Utils.setPKMode(ICE_Config.getDroplistValue("pkMode")) -- 设置PK模式
    ICE_Task.taskDailyAtonce() 	-- 立即日常任务
	ICE_OnHook.hook()				-- 打怪挂机
	ICE_OnHook.inplaceOnHook() 	-- 原地挂机
	ICE_Utils.block("没有开启任何功能，只能站在原地发呆~_~")
end

-- 准备工作-叮当猫临时验证
ICE_System.prepare = function()
	local initResult = DDMControl.初始化("api.privateapi.xyz","9000","166cddc7-e2d3-46d5-8e9f-a9740e1627a2","6167dbaf-fbb2-43e3-9de4-ab2455ccb004","")
	while initResult == false do
		initResult = DDMControl.初始化("api.privateapi.xyz","9000","166cddc7-e2d3-46d5-8e9f-a9740e1627a2","6167dbaf-fbb2-43e3-9de4-ab2455ccb004","")
		sleep(500)
	end
	-- 心跳回调
	local function heartbeatCallback(msg)
		if msg.code == 1 then
			ICE_Utils.logger("心跳正常", false)
		elseif msg.code == -5 then
			ICE_Utils.logger("心跳异常，状态码：-5", false)
			ICE_Utils.showTipsLarge("请重新登录,一般是卡密被禁用,删除,设备被解绑!")
			setTimer(exitScript, 3000)
		elseif msg.code == -8 then
			ICE_Utils.logger("心跳异常，卡密到期", false)
			ICE_Utils.showTipsLarge("卡密到期")
			setTimer(exitScript, 3000)
		elseif msg.code == -9999 then
			ICE_Utils.logger("心跳失败,网络错误!", false)
		elseif msg.code == -6666 then
			ICE_Utils.logger("有人尝试破解卡密系统!" .. msg.cdkey, false)
		else
			ICE_Utils.logger("未知错误!" .. msg.msg, false)
		end
	end
	if ICE_UI.show() then   -- 显示UI
		DDMControl.云控_连接云控系统()
		local verifyResult = DDMControl.卡密_卡密登录("166cddc7-e2d3-46d5-8e9f-a9740e1627a2","89aa0262-0343-412f-afbc-c45256ec3834","LS66nkeh",heartbeatCallback,true,false,60)
		if verifyResult.code == -5 then
			ICE_Utils.blockLarge("卡密错误，请检查卡密")
		elseif verifyResult.code == 0 then
			ICE_Utils.blockLarge("卡密被禁用")
		elseif verifyResult.code == -1 then
			ICE_Utils.blockLarge("网络错误，请检查网络！")
		elseif verifyResult.code == 1 then
			ICE_Utils.logger("卡密验证成功，到期时间：" .. verifyResult.endTime, false)
			ICE_Utils.showTipsLarge("卡密验证成功\n到期时间：" .. verifyResult.endTime)
			sleep(3000)
			if ICE_Login.launchGame() then
				ICE_System.gameSettings()
			else
				ICE_Utils.block("加载游戏失败！！！")
			end
		elseif verifyResult.code == -9 then
			ICE_Utils.blockLarge("卡密授权窗口达到上限！")
		elseif verifyResult.code == -7 then
			ICE_Utils.blockLarge("卡密过期！")
		elseif verifyResult.code == -6666 then
			ICE_Utils.blockLarge("有人尝试破解卡密系统！" .. verifyResult.cdkey)
		else
			ICE_Utils.blockLarge("未知错误！" .. verifyResult.msg)
		end
	else
		exitScript()
	end
end