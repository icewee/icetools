--[===[-------------------------------------------------------------------------------------------------

★★★★★★★★★★ ICE工具包 ★★★★★★★★★★

--------------------------------------------------------------------------------------------------------
文件名称：Icesoft
文件说明：泡椒云验证 + 蓝奏云热更(远昔直链API)
----------------------------------------------------------------------------------------------------]===]

--[===[
方法名称: Icesoft.init 初始化泡椒云验证参数
语法: Icesoft.init(appKey, appSecret, apiHost, tokenFilename)
appKey：应用编码
appSecret: 应用秘钥
apiHost: 请求地址前缀，如：api2
tokenFilename: 登录令牌保存文件名
返回值: 无

方法名称: Icesoft.setCard 设置当前要验证的卡密
语法: Icesoft.setCard(card)
card: 卡密
返回值: 无

方法名称: Icesoft.verify 校验卡密
语法: Icesoft.verify()
返回值: 
code: 0-正常\其他值-异常
message: 错误信息

方法名称: Icesoft.heartbeatFailed 设置心跳异常回调函数
语法: Icesoft.heartbeatFailed(callback)
callback: 回调函数名称，传入参数为心跳结果
返回值: 无

方法名称: Icesoft.getCardState 获取卡密状态
语法: Icesoft.getCardState()
返回值:
state: 卡密状态值

方法名称: Icesoft.stopHeartbeat 停止发送心跳
语法: Icesoft.stopHeartbeat()
返回值: 无

方法名称: Icesoft.update 热更
语法: Icesoft.update(localVersion, downloadUrl)
localVersion: 本地版本，必须是数字，可以是小数
downloadUrl: 蓝奏云热更文件下载地址，必须是lr文件，由于蓝奏云无法上传lr文件，需将lr改为zip
返回值: 无
]===]

-------------------------------------------Icesoft Tester-------------------------------------------
-- 1.添加依赖
require("Icesoft")

--[===[-- 2.初始化
Icesoft.debug = true
Icesoft.init("cj9itr3dqusottbbqqm0", "aSAF1or06Tt7MaFOkVGL8B0N1LO9MyDD", "api", "card_test")

EXIT_SCRIPT = false -- 测试变量

-- 3.可选-心跳失败回调函数
Icesoft.heartbeatFailed(function(ret)
	print("[MAIN] heartbeat failed callback")
	print(ret.code)
    if 10210 == ret.code then
    	print("[MAIN] 卡密已过期，停止脚本运行")
        EXIT_SCRIPT = true
    elseif 10212 == ret.code then
    	print("[MAIN] 卡密已冻结，停止脚本运行")
		EXIT_SCRIPT = true
    end
    print(ret.message)
end)

-- 4.设置卡密
Icesoft.setCard("HX10612647")

-- 5.验证卡密
local code, message = Icesoft.verify()
if 0 == code then
	local state
	while true do
    	state = Icesoft.getCardState()
		print("[MAIN] Card state code is " .. tostring(state))
        if 0 ~= state then
        	print("[MAIN] Send stop heartbeat signal.")
        	Icesoft.stopHeartbeat()
        end
        if EXIT_SCRIPT then
        	break
        end
		sleep(10000)
	end
else
	print(code)
	print(message)
end]===]
-------------------------------------------Icesoft Tester-------------------------------------------

-------------------------------------------科御热更公告模板-------------------------------------------
--[===[
最多行数：6；文字个数不超过15字，超出会换行影响美观；

[增加] 领取每日福利、免费找回经验；
[增加] 一键起号结束后找军需官领取奖励；
[优化] 古镜之谜、天降宝箱；
[优化] 封魔任务过程中可以参加定时活动；
[优化] 封魔任务进图后增加自动进组功能；
[优化] 封魔任务增加放弃任务地图选项；
]===]

Icesoft.debug = true
Icesoft.update(1.2, "https://wws.lanzoul.com/yshxlr") -- 热更

--local ret = Icesoft.lanzoul("https://wws.lanzoul.com/paddleapk")
--print(ret)