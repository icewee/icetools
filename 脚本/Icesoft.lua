--[===[
	1.基于泡椒云卡密验证
	2.基于蓝奏云热更
]===]
Icesoft = {
	version = 2.2,
	debug = false,
	appKey = nil,
    appSecret = nil,
    apiHost = nil,
    tokenFilename = nil, -- 登录令牌存储文件名
    card = nil, -- 当前卡密
    cardStateCode = 0, -- 卡密状态码
    currentLoginToken = nil, -- 当前登录TOKEN，用于心跳请求
    heartbeatOn = true, -- 心跳开关
    heartbeatFailedCallbackFunction = nil -- 心跳失败回调函数
}

-- 日志级别
Icesoft.DEBUG = 2
Icesoft.INFO = 3
Icesoft.WARN = 4
Icesoft.ERROR = 5

-- 泡椒云接口地址
Icesoft.LoginPath = "/v1/card/login"
Icesoft.LogoutPath = "/v1/card/logout"
Icesoft.HeartbeatPath = "/v1/card/heartbeat"

-- 蓝奏云API接口地址
Icesoft.ApiUrl = {}
Icesoft.ApiUrl.Yuanxi = "https://yuanxiapi.cn/api/lanzou/" -- 远昔API

Icesoft.downloadUrl = ""
Icesoft.closeBox = false
Icesoft.boxId = "更新提示"
Icesoft.latestVersion = 0 -- 最新版本号
Icesoft.forceUpdate = true

Icesoft.TokenFileDir = "/mnt/sdcard/"

----------------------------------------------泡脚云验证----------------------------------------------
-- [开放接口]初始化
function Icesoft.init(appKey, appSecret, apiHost, tokenFilename)
	Icesoft.logger(Icesoft.INFO, "Icesoft plugin. version: " .. Icesoft.version)
	if Icesoft.isBlank(appKey) then
    	Icesoft.logger(Icesoft.ERROR, "appKey can not be empty.")
        exitScript()
    end
    if Icesoft.isBlank(appSecret) then
		Icesoft.logger(Icesoft.ERROR, "appSecret can not be empty.")
		exitScript()
	end
    if Icesoft.isBlank(tokenFilename) then
		Icesoft.logger(Icesoft.ERROR, "tokenFilename can not be empty.")
		exitScript()
	end
	Icesoft.appKey = appKey
    Icesoft.appSecret = appSecret
    Icesoft.apiHost = apiHost
    Icesoft.tokenFilename = tokenFilename
    Icesoft.logger(Icesoft.INFO, "init successfully.")
end

-- [开放接口]设置卡密
function Icesoft.setCard(card)
	if Icesoft.isBlank(card) then
    	card = ""
    else
    	card = Icesoft.trim(card)
    end
	Icesoft.logger(Icesoft.DEBUG, "card is " .. card)
	Icesoft.card = card
end

-- [开放接口]校验卡密
function Icesoft.verify()
	local code, message = 0, ""
	if Icesoft.isBlank(Icesoft.card) then
		code, message = 403, Icesoft.getMessage(403)
	else
		-- 1.读取配置文件注销登录
		local loginToken = Icesoft.readToken()
		if Icesoft.isNotBlank(loginToken) then
			Icesoft.logout(loginToken)
		end
		-- 2.登录验证
		local succeed, token, msg = Icesoft.login()
		if succeed then
			beginThread(Icesoft.heartbeatThread) -- 启动心跳线程
		else
			code = -1
		end
        message = msg
	end
    return code, message
end

-- [开放接口]心跳失败回调函数
function Icesoft.heartbeatFailed(callback)
    Icesoft.heartbeatFailedCallbackFunction = callback
end

-- [开放接口] 获取卡密状态码
function Icesoft.getCardState()
	return Icesoft.cardStateCode
end

-- [开放接口] 停止发送心跳
function Icesoft.stopHeartbeat()
	Icesoft.heartbeatOn = false
end

-- 登录
Icesoft.login = function(heartbeat)
	local token, message = "UNKNOW", "" 
	local host = Icesoft.getApiHost()
	local url = Icesoft.getRequestUrl(host, Icesoft.LoginPath)
    Icesoft.logger(Icesoft.DEBUG, "Login url: " .. url)
	local deviceId = Icesoft.getDeviceUid()
	local params = Icesoft.loginParams(host, deviceId)
    Icesoft.logger(Icesoft.DEBUG, "Login params: " .. params)
	local succeed, ret = pcall(function()
		local response = httpPost(url, params, 10)
        if Icesoft.isNotBlank(response) then
        	Icesoft.logger(Icesoft.DEBUG, "Login response: " .. response)
        end
		return jsonLib.decode(response)
	end)
	if succeed then -- 请求成功
		local code = ret.code
		local msg = ret.message
		if 0 == code then -- 验证通过
			local result = ret.result
			local expires = result.expires -- 到期显示时间
			local expiresTs = result.expires_ts -- 到期时间戳
			token = result.token
			Icesoft.currentLoginToken = token -- 缓存TOKEN
			if os.time() >= expiresTs then -- 已到期
				succeed = false
				message = Icesoft.getMessage(10210, msg)
			else -- 卡密有效
				message = Icesoft.getLoginMessage(expires)
                Icesoft.writeToken(token) -- 写token进文件
			end
		else
			succeed = false
			message = Icesoft.getMessage(code, msg)
            -- 只处理卡密被冻结场景，发送心跳时卡密冻结返回10214-登录状态失效，调用登录才返回10212-卡密冻结
            if heartbeat then -- 心跳调用
            	if 503 ~= code then -- 503是服务器连不上
					Icesoft.heartbeatFailedCallback(ret)
				end
            end
		end
        Icesoft.cardStateCode = code -- 更新卡密状态码
	else -- 请求失败
		Icesoft.logger(Icesoft.ERROR, "connect Paojiaoyun server failed.")
		message = Icesoft.getMessage(503, msg)
	end
	return succeed, token, message
end

-- 拼接登录验证参数
Icesoft.loginParams = function(host, deviceId)
	local params = "app_key=" .. Icesoft.appKey
	params = params .. "&card=" .. Icesoft.card
	params = params .. "&device_id=" .. deviceId
	params = params .. "&nonce=" .. Icesoft.guid()
	params = params .. "&timestamp=" .. Icesoft.timestamp()
	local sign = Icesoft.sign(host, Icesoft.LoginPath, params)
	params = params .. "&sign=" .. sign
	return params
end

-- 登出
Icesoft.logout = function(token)
	local host = Icesoft.getApiHost()
	local url = Icesoft.getRequestUrl(host, Icesoft.LogoutPath)
    Icesoft.logger(Icesoft.DEBUG, "Logout url: " .. url)
	local params = Icesoft.logoutParams(host, token)
    Icesoft.logger(Icesoft.DEBUG, "Logout params: " .. params)
	local succeed, ret = pcall(function()
		local response = httpPost(url, params, 10)
        if Icesoft.isNotBlank(response) then
			Icesoft.logger(Icesoft.DEBUG, "Logout response: " .. response)
		end
		return jsonLib.decode(response)
	end)
	if succeed then -- 请求成功
		return ret
	else
		return {code = 503, message = "注销登录失败"}
	end
end

-- 拼接注销登录参数
Icesoft.logoutParams = function(host, token)
	local params = "app_key=" .. Icesoft.appKey
	params = params .. "&card=" .. Icesoft.card
	params = params .. "&nonce=" .. Icesoft.guid()
	params = params .. "&timestamp=" .. Icesoft.timestamp()
	params = params .. "&token=" .. token
	local sign = Icesoft.sign(host, Icesoft.LogoutPath, params)
	params = params .. "&sign=" .. sign
	return params
end

-- 发送心跳
Icesoft.heartbeat = function(token)
	local host = Icesoft.getApiHost()
	local url = Icesoft.getRequestUrl(host, Icesoft.HeartbeatPath)
    Icesoft.logger(Icesoft.DEBUG, "Heartbeat url: " .. url)
	local params = Icesoft.heartbeatParams(host, token)
    Icesoft.logger(Icesoft.DEBUG, "Heartbeat params: " .. params)
	local succeed, ret = pcall(function()
		local response = httpPost(url, params)
        if Icesoft.isNotBlank(response) then
			Icesoft.logger(Icesoft.DEBUG, "Heartbeat response: " .. response)
		end
		return jsonLib.decode(response)
 	end)
	if succeed then -- 请求成功
    	return ret.code, ret.message, ret
  	else
    	return 503, "发送心跳失败"
  	end
end

-- 心跳请求参数
Icesoft.heartbeatParams = function(host, token)
	local params = "app_key=" .. Icesoft.appKey
	params = params .. "&card=" .. Icesoft.card
	params = params .. "&nonce=" .. Icesoft.guid()
	params = params .. "&timestamp=" .. Icesoft.timestamp()
	params = params .. "&token=" .. token
	local sign = Icesoft.sign(host, Icesoft.HeartbeatPath, params)
	params = params .. "&sign=" .. sign
	return params
end

-- 调用用户设置的心跳失败回调函数
Icesoft.heartbeatFailedCallback = function(ret)
	if "function" == type(Icesoft.heartbeatFailedCallbackFunction) then
    	Icesoft.heartbeatFailedCallbackFunction(ret)
    end
end

-- 心跳线程
Icesoft.heartbeatThread = function()
	local lastHeartbeatTime = 0
	local code, message, ret = 0
    Icesoft.logger(Icesoft.INFO, "heartbeat thread started.")
	while true do
    	if Icesoft.heartbeatOn == false then
        	Icesoft.logger(Icesoft.INFO, "heartbeat thread stopped.")
            break
        end
		if os.time() - lastHeartbeatTime >= 60 then
			code, message, ret = Icesoft.heartbeat(Icesoft.currentLoginToken)
			if 0 ~= code then
				if 10214 == code then -- 登录状态已失效
					Icesoft.logger(Icesoft.WARN, "heartbeat failed, relogin.")
					Icesoft.login(true) -- 心跳调用标识
                else
                	Icesoft.logger(Icesoft.WARN, "heartbeat failed. " .. message)
                    if 503 ~= code then -- 503为服务器无法连接
                    	Icesoft.heartbeatFailedCallback(ret)
                    end
                    Icesoft.cardStateCode = code -- 更新卡密状态码
				end
			end
			lastHeartbeatTime = os.time()
		end
		sleep(10 * 1000)
	end
end

-- 动态拼接验证服务器地址
Icesoft.getApiHost = function()
	local prefix = Icesoft.apiHost
  	if Icesoft.isBlank(prefix) then
    	prefix = "api2"
  	end
  	return prefix .. ".paojiaoyun.com"
end

-- 生成签名
Icesoft.sign = function(host, path, params)
	local text = "POST" .. host .. path .. params .. Icesoft.appSecret
  	return MD5(text)
end

-- 拼接请求地址
Icesoft.getRequestUrl = function(host, path)
  	return "http://" .. host .. path
end

-- 解析状态码
Icesoft.getMessage = function(code, msg)
	local message = ""
    if 400 == code then
    	message = "卡 密 验 证 失 败\n验 证 服 务 请 求 参 数 错 误"
    elseif 403 == code then -- 自定义错误，卡密为空
    	message = "卡 密 验 证 失 败\n卡 密 不 能 为 空"
  	elseif 404 == code then
    	message = "卡 密 验 证 失 败\n卡 密 错 误 或 已 过 期 清 除"
    elseif 500 == code then -- 服务器内部错误
        message = "卡 密 验 证 失 败\n验证服务器内部错误，请联系开发者"
  	elseif 503 == code then -- 自定义错误，服务器无法访问，被攻击
    	message = "卡 密 验 证 失 败\n无法连接验证服务器，请联系开发者"
  	elseif 10010 == code then -- 无效的签名
    	message = "卡 密 验 证 失 败\n模拟器用户尝试电脑校时并重启模拟器"
  	elseif 10011 == code then -- 签名已过期
    	message = "卡 密 验 证 失 败\n模拟器用户尝试电脑校时并重启模拟器"
  	elseif 10013 == code then -- 时间戳大于当前服务器时间
    	message = "卡 密 验 证 失 败\n模拟器用户尝试电脑校时并重启模拟器"
  	elseif 10210 == code then -- 卡密已过期
    	message = "卡 密 验 证 失 败\n当 前 卡 密 已 经 过 期"
  	elseif 10213 == code then -- 卡密超过多开上限
    	message = "卡 密 验 证 失 败\n当前卡密使用设备数已超出最大限制"
  	elseif 10217 == code then -- 超出可绑定设备上限
    	message = "卡 密 验 证 失 败\n当前卡密使用设备数已超出最大限制"
  	else
    	message = "卡 密 验 证 失 败\n" .. msg
  	end
  	return message
end

-- 生成登录成功提示信息
Icesoft.getLoginMessage = function(expires)
	local message = "卡 密 验 证 成 功\n到 期 时 间 ：" .. expires
  	return message
end

-- 读取本地文件中的token
Icesoft.readToken = function()
	local filepath = Icesoft.TokenFileDir .. Icesoft.tokenFilename
    local token = readFile(filepath)
    if Icesoft.isNotBlank(token) then
   		Icesoft.logger(Icesoft.DEBUG, "read token[" .. token .. "] from file[" .. filepath .. "].")
    end
    return token
end

-- 将token写入本地文件中
Icesoft.writeToken = function(token)
	local filepath = Icesoft.TokenFileDir .. Icesoft.tokenFilename
    Icesoft.logger(Icesoft.DEBUG, "write token[" .. token .. "] into file[" .. filepath .. "].")
    writeFile(filepath, token, false)
end
----------------------------------------------蓝奏云热更----------------------------------------------
-- [开放接口] 热更
function Icesoft.update(localVersion, downloadUrl)
	Icesoft.logger(Icesoft.INFO, "Icesoft plugin. version: " .. Icesoft.version)
	local code, json = Icesoft.parseDownloadUrl(downloadUrl)
    if -1 ~= code then
    	code = tonumber(json["code"])
        if 200 == code then
            local filename = json["name"]
            Icesoft.downloadUrl = json["download"]
            Icesoft.latestVersion = Icesoft.parseLatestVersion(filename)
            Icesoft.logger(Icesoft.INFO, "Latest version: " .. Icesoft.latestVersion .. ". Local version: " .. localVersion)     
            if Icesoft.latestVersion > localVersion then
            	Icesoft.showUpdatePromptBox()
            else
            	Icesoft.showTipsLarge("当 前 已 是 最 新 版 本")
				sleep(1000)
            end
        else
        	local msg = json["msg"]
            if Icesoft.isBlank(msg) then
            	msg = "没有文字信息"
            end
            Icesoft.logger(Icesoft.WARN, "LanzApi response code: " .. code .. ". msg: " .. msg)
        end
    else
    	Icesoft.logger(Icesoft.ERROR, "LanzApi invoke failed.")
    end
end

-- 解析下载地址
-- code=200时，返回json属性必须包含name和download
Icesoft.parseDownloadUrl = function(originUrl)
	Icesoft.logger(Icesoft.DEBUG, "Origin url: " .. originUrl)
    -- 0.本地解析
    local code, json = Icesoft.parseDownloadUrlLocal(originUrl)
    if code ~= 0 then -- 失败
    	-- 1.远昔API
		local requestUrl = Icesoft.ApiUrl.Yuanxi .. "?url=" .. originUrl
		Icesoft.logger(Icesoft.DEBUG, "Request url: " .. requestUrl)
		local succeed, ret = pcall(function()
			local response = httpGet(requestUrl, 3)
			if Icesoft.isNotBlank(response) then
				Icesoft.logger(Icesoft.DEBUG, "YuanxiApi response: " .. response)
			end
			return jsonLib.decode(response)
		end)
        if succeed then -- 请求成功
			return 0, ret
		else
			return -1, "YuanxiApi invoke failed."
		end
    end
    return code, json
end

-- 借助本地蓝奏云直链解析接口
Icesoft.parseDownloadUrlLocal = function(originUrl)
	local ret = Icesoft.lanzoul(originUrl)
    local code = ret.code
    local retCode = 0
    if code == 500 then
    	Icesoft.logger(Icesoft.ERROR, ret.msg)
    	retCode = -1
    end
    return retCode, ret
end

-- 解析文件名获得版本号
-- 示例：傲视-剑来传世_1.27.zip
Icesoft.parseLatestVersion = function(filename)
	local version = -1
    if Icesoft.isNotBlank(filename) then
    	filename = Icesoft.trim(filename)
        if Icesoft.indexOf(filename, "_") ~= -1 then -- 下划线后是版本号
        	local nameArray = splitStr(filename, "_")
           	if #nameArray >= 2 then
            	if Icesoft.indexOf(filename, ".") ~= -1 then
            		local verArray = splitStr(nameArray[2], ".zip")
                    if #verArray >= 1 and Icesoft.isNumberStr(verArray[1]) then
                    	version = tonumber(verArray[1])
                    end
                end
            end
        end
    end
    return version
end

-- 热更：下载、安装、重启脚本
Icesoft.hot = function()
	local temppath = "/mnt/sdcard/" .. systemTime() .. ".lr"
	local function progress(percent)
		Icesoft.showTipsLarge("下 载 进 度 ：" .. percent .. " %")
	end
	local downloadCode = downloadFile(Icesoft.downloadUrl, temppath, progress)
	if 0 == downloadCode then
    	Icesoft.showTipsLarge("下 载 完 成，安 装 脚 本")
        sleep(1000)
		installLrPkg(temppath)
        Icesoft.showTipsLarge("安 装 完 成，重 启 脚 本")
        sleep(1000)
		delfile(temppath)
		restartScript()
    else
    	Icesoft.showTipsLarge("脚 本 下 载 失 败")
        delfile(temppath)
        sleep(1000)
	end
end

-- 按钮点击事件
Icesoft.buttonHandler = function(buttonNo)
	if 1 == buttonNo then
		if Icesoft.forceUpdate then -- 强更
			exitScript()
        else
        	Icesoft.closeUpdatePromptBox() -- 关闭弹窗
		end
	else
    	Icesoft.closeUpdatePromptBox() -- 关闭弹窗
        Icesoft.showTipsLarge("准 备 更 新 脚 本 , 请 耐 心 等 待")
        sleep(1000)
        Icesoft.hot() -- 下载、安装、重启脚本
	end	
end

-- 显示更新提示框
Icesoft.showUpdatePromptBox = function()
	local dynamicButton
	if Icesoft.forceUpdate == true then
		dynamicButton = '退        出'
	else
		dynamicButton = '忽        略'
	end
	--标题
	local title = Icesoft.boxId
	ui.newLayout(title, 620, -2)
	ui.setTextColor(title, "#FFFFFFFF")
	ui.setTitleBackground(title, "#FF80B8E2")
	-- 更新内容
	ui.addLine(title, "lineTop", -1, 1)
	ui.newRow(title, "row")
    ui.newRow(title, "row")
	ui.addTextView(title, "versionInfo", "								发 现 新 版 本 v" .. Icesoft.latestVersion, -1, 40)    
    ui.newRow(title, "row")
    ui.newRow(title, "row")
    ui.newRow(title, "row")
	ui.addLine(title, "lineBottom", -1, 1)
	ui.newRow(title, "row")
	-- 按钮
	ui.addButton(title, "dynamicButton", dynamicButton, 272)
	ui.addButton(title, "updateButton", "更        新", 272)
	ui.setBackground("dynamicButton", "#FFDC323F")
	ui.setBackground("updateButton", "#FF80B8E2")
	ui.setOnClick("dynamicButton", "Icesoft.buttonHandler(1)")
	ui.setOnClick("updateButton", "Icesoft.buttonHandler(2)")
	ui.show(title, false)
    
    local timeoutUpdate = true -- 超时(30s)结束自动更新
    for i = 150, 1, -1 do
    	if i % 5 == 0 then -- 1秒一次
        	ui.setText("updateButton", "更        新(" .. math.tointeger((i / 5)) .. "s)")
        end
    	if Icesoft.closeBox then -- 自动更新被打断
        	timeoutUpdate = false
			break
		end
        sleep(200)
    end
    if timeoutUpdate then
    	Icesoft.buttonHandler(2)
    end
end

-- 关闭更新提示框
Icesoft.closeUpdatePromptBox = function()
	Icesoft.closeBox = true
	ui.dismiss(Icesoft.boxId) -- 关闭更新提示框
end
----------------------------------------------蓝奏解析----------------------------------------------
-- [开放接口]解析蓝奏云下载地址
function Icesoft.lanzoul(url)
	Icesoft.logger(Icesoft.DEBUG, "Invoke local lanzoul url parse API.")
	local host, path = url:match("(https?://%w+%.%w+%.%w+)/(.*)")
	local requestSucceed, downloadLink = pcall(function ()
		local response = httpGet(url, 3)
		return response
	end)
	if requestSucceed then
		local jsda, down
		local ret = string.find(downloadLink, "取消分享", 1)
		if ret == nil then
			local jsurl = string.match(downloadLink, "/iframe>(.-)/iframe")
			if jsurl == nil then
				jsurl = string.match(downloadLink, "iframe(.-)/iframe")
			end
			if jsurl == nil then
				jsurl = string.match(downloadLink, "infos(.-)infomores")
			end
			jsurl = string.match(jsurl, 'src="(.-)"')
			local filename = string.match(downloadLink, 'id="filenajax">(.-)<')
			if filename == nil then
				filename = string.match(downloadLink, '20px 0px;">(.-)<')
			end
            requestSucceed, jsda = pcall(function ()
            	local response = httpGet(host .. jsurl, 3)
                return response
            end)
            if requestSucceed then
            	local id = string.match(jsda, "?file=(.-)',")
				local sign = string.match(jsda, "sign':'(.-)',")
                local websignkey = string.match(jsda, "aihidcms = '(.-)'")
				local data = "action=downprocess&signs=%3Fctdf&websign=&websignkey=" .. websignkey .. "&ves=1&sign=" .. sign
                local head = "Content-Type: application/x-www-form-urlencoded\nOrigin: " .. host .. "\nReferer: " .. url
				requestSucceed, down = pcall(function ()
					local response = httpPost(host .. "/ajaxm.php?file=" .. id, data, 3, head)
					return jsonLib.decode(response)
				end)
                if requestSucceed then
                	local dwurl = down.dom .. "/file/" .. down.url
                    local downloadUrl = Icesoft.lanzoulDirectLink(dwurl)
                    if Icesoft.isNotBlank(downloadUrl) then
                    	return {code = 200, name = filename, download = downloadUrl}
                    else
                    	return {code = 500, msg = "Lanzoul direct link parse failed."}
                    end
                else
                	return {code = 500, msg = "Lanzoul download invoke failed."}
                end
            else
            	return {code = 500, msg = "Lanzoul invoke failed."}
            end	
		else
			return {code = 404, msg = "Lanzoul file not found."}
		end
	else
		return {code = 500, msg = "Lanzoul url invoke failed."}
	end	
end

-- 获取真实下载地址
Icesoft.lanzoulDirectLink = function(originUrl)
	local succeed, url = pcall(Icesoft.lanzoulRedirectUrl, originUrl)
	if succeed == false then
		url = nil
	end
	return url
end

-- 根据请求地址获取重定向后的真实下载地址
Icesoft.lanzoulRedirectUrl = function(originUrl)
	import("java.lang.*")
	import("java.net.HttpURLConnection")
	import("java.net.URL")
	local url = URL(originUrl)
	local conn = url.openConnection()
	conn.setRequestProperty("Accept-Language", "zh-CN,zh;q=0.8,zh-TW;q=0.7,zh-HK;q=0.5,en-US;q=0.3,en;q=0.2")
	conn.setInstanceFollowRedirects(false)
	conn.setConnectTimeout(3000)
	local redirectUrl = conn.getHeaderField("Location")
	conn.disconnect()
	return redirectUrl
end
----------------------------------------------通用函数----------------------------------------------
-- 中心显示大字提示信息
Icesoft.showTipsLarge = function(tips)
	toast(tips, 0, 0, 16)
end

-- 生成请求时间戳
Icesoft.timestamp = function()
	local timestamp = nil
	-- 格式：2023-12-24_20-55-08
	local workTimeStr = getNetWorkTime()
    if workTimeStr ~= nil then
    	local array = splitStr(workTimeStr, "_")
        if #array == 2 then
        	local dateStr = array[1]
            local timeStr = array[2]
            local dateArray = splitStr(dateStr, "-")
            local timeArray = splitStr(timeStr, "-")
            if #dateArray == 3 and #timeArray == 3 then
            	local specifyYear, specifyMonth, specifyDay = tonumber(dateArray[1]), tonumber(dateArray[2]), tonumber(dateArray[3])
                local specifyHour, specifyMinute, specifySecond = tonumber(timeArray[1]), tonumber(timeArray[2]), tonumber(timeArray[3])
            	local datetime = {year = specifyYear, month = specifyMonth, day = specifyDay, hour = specifyHour, min = specifyMinute, sec = specifySecond}
               	timestamp = os.time(datetime)
				Icesoft.logger(Icesoft.DEBUG, "Net timestamp: ".. timestamp)
            end
        end
    end
    if timestamp == nil then
    	timestamp = os.time() - 10
        Icesoft.logger(Icesoft.DEBUG, "OS timestamp: " .. timestamp)
    end
  	return timestamp
end

-- 生成设备标识，不能保证唯一，因为目前方案补绑定设备通过心跳
-- 目的只是获取一个不为nil的值，因为游戏安卓系统deviceId是nil
Icesoft.getDeviceUid = function()
	local deviceUid = getDeviceId()
  	if Icesoft.isBlank(deviceUid) then
    	deviceUid = getId()
    	if Icesoft.isBlank(deviceUid) then
      		deviceUid = getDevice()
      		if Icesoft.isBlank(deviceUid) then
        		deviceUid = "NIL" .. os.time()
      		end
    	end
  	end
  	return deviceUid
end

-- 生成唯一标识
Icesoft.guid = function()
	local arr = {
    	"0","1","2","3","4","5","6","7","8","9",
    	"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z",
    	"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z",
  	}
	local rndStr = ""
	local maxIdx = #arr
  	local idx
  	for i = 1, 32, 1 do
        idx = rnd(1, maxIdx)
      	rndStr = rndStr .. arr[idx]
  	end
	return rndStr
end

-- 去空格
Icesoft.trim = function(str)
   if str == nil then
        str = ""
    end
	return string.gsub(str, "^%s*(.-)%s*$","%1")
end

-- 判断一个字符串是否为空串或nil
Icesoft.isBlank = function(str)
    if str == nil then
        return true
    end
    if Icesoft.trim(str) == "" then
        return true
    end
    return false
end

-- 判断一个字符串是否不为空串或nil
Icesoft.isNotBlank = function(str)
    return not Icesoft.isBlank(str)
end

-- 数字字符串判断
Icesoft.isNumberStr = function(str)
	local is = false
    if Icesoft.isNotBlank(str) then
    	if tonumber(str) ~= nil then
        	is = true
        end
    end
    return is
end

-- 用于判断字符串中是否包含指定子串或字符
Icesoft.indexOf = function(string, pattern)
	local index = -1
    local ret = string.find(string, pattern)
    if ret ~= nil then
    	index = ret
    end
    return index
end

-- 打印日志
Icesoft.logger = function(level, message)
	local prefix = "[Icesoft] [DEBUG] - "
    if Icesoft.INFO == level then
    	prefix = "[Icesoft] [INFO] - "
    elseif Icesoft.WARN == level then
    	prefix = "[Icesoft] [WARN] - "
    elseif Icesoft.ERROR == level then
        prefix = "[Icesoft] [ERROR] - "
    end
	message = prefix .. message
	if Icesoft.DEBUG == level then
    	if Icesoft.debug then
			print(message)
			console.println(level, message)
			writeLog(message)
		end
	else
    	print(message)
		console.println(level, message)
		writeLog(message)
    end
end