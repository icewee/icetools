--[===[-------------------------------------------------------------------------------------------------

★★★★★★★★★★ 飞悦传世疾速版源码备份 ★★★★★★★★★★

----------------------------------------------------------------------------------------------------]===]

-- 准备工作
ICE_System.prepare = function()
	if ICE_UI.show() then   -- 显示UI
		km = ICE_Config.cardNo()
		if ICE_Utils.isBlank(km) then
			ICE_Utils.blockLarge("卡 密 验 证 失 败\n请 输 入 已 购 买 的 卡 密 ！")
		else
			as.退出上次心跳()
			卡密token = as.卡密登陆(km)
			if 卡密token ~= nil then
				as.开启心跳线程(km, 卡密token)
			else
				ICE_Utils.blockLarge("卡 密 验 证 失 败")
			end
		end
		if ICE_Login.launchGame() == false then
			ICE_Utils.blockLarge("加 载 游 戏 失 败 ！")
		end
	else
		exitScript()
	end
end

-- 开始运行脚本
ICE_System.start = function()
	ICE_System.prepare()    		-- 初始化
    ICE_System.gameSettings()	-- 游戏设置
	ICE_Package.checkStone() 		-- 检查石头
    ICE_Task.taskDailyAtonce() 	-- 立即日常任务
	ICE_OnHook.setPKMode() 		-- 设置PK模式
	ICE_OnHook.hook()				-- 打怪挂机
    ICE_OnHook.inplaceOnHook() 	-- 原地挂机
	ICE_Utils.block("没有开启任何功能，只能站在原地发呆~_~")
end